import logging
from database import db
from telegram.ext import CommandHandler, CallbackQueryHandler
from telegram.ext import MessageHandler
from telegram.ext import Updater
from telegram.ext import Filters

from settings import TG_TOKEN
from settings import TG_API_URL
from handlers import *

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO,
                    filename='bot.log'
                    )


# Создаем (объявляем) функцию main, которая соединяется с платформой Telegram
def main():
    # описываем функцию (тело функции)
    # создадим переменную my_bot, с помощью которой будем взаимодействовать с нашим ботом
    my_bot = Updater(TG_TOKEN, TG_API_URL, use_context=True)
    logging.info('Start bot')
    my_bot.dispatcher.add_handler(CommandHandler('start', sms))  # обработчик команды start
    my_bot.dispatcher.add_handler(MessageHandler(Filters.regex('Начать'), sms))  # обрабатываем текс кнопки
    my_bot.dispatcher.add_handler(MessageHandler(Filters.regex('Анекдот'), get_anecdote))  # обрабатываем текс кнопки
    my_bot.dispatcher.add_handler(MessageHandler(Filters.contact, get_contact))  # обработчик полученного контакта
    #my_bot.dispatcher.add_handler(MessageHandler(Filters.location, get_location))  # обработчик полученной геопозиции

    my_bot.dispatcher.add_handler(CallbackQueryHandler(inline_button_pressed))

    my_bot.dispatcher.add_handler(
        ConversationHandler(entry_points=[MessageHandler(Filters.regex("Получить QR Code 🖌"), get_data_start)],
                            states={
                                "user_name": [MessageHandler(Filters.text, anketa_get_name)],
                                "user_surname": [MessageHandler(Filters.text, anketa_get_surname)],
                                'user_lastname': [MessageHandler(Filters.text, anketa_get_last_name)],
                                'user_email': [MessageHandler(Filters.text, anketa_get_email)],
                                "change": [MessageHandler(Filters.regex('Фамилия|Имя|Отчество|Email|Продолжить'), choice_change)],
                                "continue": [MessageHandler(Filters.text(['Пропуск в магазин','Рабочий пропуск','Одноразовый пропуск']), choice_type)],
                                "location": [MessageHandler(Filters.location, location)],
                                'shop-time': [MessageHandler(Filters.text, time)],
                                "send-qr": [MessageHandler(Filters.text, send_qr)],
                                'user_data': [MessageHandler(Filters.text, anketa_get_persdata)],
                                'transport': [MessageHandler(Filters.text(['Личный транспорт/Служебный транспорт', 'Общественный транспорт']), anketa_get_transport)],
                                'aim': [MessageHandler(Filters.text, anketa_get_aim)],
                                'work_name': [MessageHandler(Filters.text, anketa_get_workname)],
                                'work_inn': [MessageHandler(Filters.text, anketa_get_workinn)]
                            },
                            fallbacks=[MessageHandler(
                                Filters.text | Filters.video | Filters.photo | Filters.document, dontknow)]
                            )
    )
    my_bot.dispatcher.add_handler(MessageHandler(Filters.text, parrot))  # обработчик текстового сообщения
    my_bot.start_polling()  # проверяет о наличии сообщений с платформы Telegram
    my_bot.idle()  # бот будет работать пока его не остановят


if __name__ == "__main__":
    main()