from telegram import ReplyKeyboardMarkup
from telegram import KeyboardButton
from math import radians, cos, sin, asin, sqrt
from models import Journey
import ast


SMILE = ['😊', '😀', '😇', '🤠', '😎', '🤓', '👶', '🧑‍🚀', '👮', '🦸', '🧟']
CALLBACK_BUTTON_PICTURE = "Действующий QR Code 🏞"
CALLBACK_BUTTON_PEN = "Получить QR Code 🖌"
CALLBACK_BUTTON_START = "Начать 🎰"
CALLBACK_BUTTON_JOKE = "Анекдот 🎭"



# функция создает клавиатуру и ее разметку
def get_keyboard_auth():
    contact_button = KeyboardButton('Отправить контакты', request_contact=True)
    my_keyboard = ReplyKeyboardMarkup([[CALLBACK_BUTTON_START, CALLBACK_BUTTON_JOKE],
                                       [CALLBACK_BUTTON_PEN, CALLBACK_BUTTON_PICTURE],[contact_button]
                                       ], resize_keyboard=True)  # добавляем кнопки
    return my_keyboard


def map_keyboard():
    location_button = KeyboardButton('Отправить геопозицию', request_location=True)
    my_keyboard = ReplyKeyboardMarkup([location_button])
    return my_keyboard


def phone_keyboard():
    contact_button = KeyboardButton('Отправить контакты', request_contact=True)
    my_keyboard = ReplyKeyboardMarkup([contact_button])
    return my_keyboard


def distance(update):
    long = update.user_data['location']['longitude']
    lat = update.user_data['location']['latitude']
    count = 0
    jours = Journey.select()
    for jour in jours:
        coords2 = ast.literal_eval(jour.end_point)

        long1 = radians(long)
        long2 = radians(coords2['longitude'])
        lat1 = radians(lat)
        lat2 = radians(coords2['latitude'])

        dlon = long2 - long1
        dlat = lat2 - lat1

        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2

        c = 2 * asin(sqrt(a))

        if c*6371 < 0.150:
            count+=1
    if count <= 3:
        return 'green'
    if 3<=count<=8:
        return 'yellow'
    if count >= 8:
        return 'red'


