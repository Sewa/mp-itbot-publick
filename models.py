from peewee import *
from database import db


class User(Model):
    first_name = CharField()
    last_name = CharField(default=None)
    surname = CharField(default=None)
    phonenumber = CharField()
    pasport = CharField()
    email = CharField()

    class Meta:
        database = db

    @staticmethod
    #Поиск существующего или создание нового пользователя
    def find_or_create_user(effective_user, message, data):
        created = False
        teleg = Telegram()
        if effective_user.last_name:
            user = User.select().where(User.phonenumber == data).first()
            print(user)
            if not user:
                user = User()
                teleg.telegram_user = effective_user.id
                user.first_name = effective_user.first_name
                user.last_name = effective_user.last_name
                user.surname = ''
                teleg.telegram_chat = message.chat_id
                user.phonenumber = data
                user.pasport = ''
                user.email = ''
                user.save()
                teleg.save()
                created = True
                return user, created
            return user, created
        elif effective_user.first_name:
            user = User().select().where(User.phonenumber == data).first()
            if not user:
                user = User()
                teleg.user_id = effective_user.id
                user.first_name = effective_user.first_name
                user.last_name = ''
                user.phonenumber=data
                user.surname = ''
                user.pasport = ''
                user.email = ''
                teleg.chat_id = message.chat_id
                user.save()
                created = True
                return user, created
            return user, created


class Journey(Model):
    user_id = ForeignKeyField(User)
    start_point = CharField()
    time = DateTimeField()
    type = CharField()
    trans_type = CharField()
    aim = CharField()
    end_point = CharField()

    class Meta:
        database = db


class Telegram(Model):
    user_id = ForeignKeyField(User)
    telegram_user = CharField()
    telegram_chat = CharField()

    class Meta:
        database = db

db.connect()
db.create_tables([User, Journey, Telegram])