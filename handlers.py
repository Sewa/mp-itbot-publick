# Импортируем необходимые компоненты
from bs4 import BeautifulSoup
from glob import glob
from random import choice
import requests
from emoji import emojize
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram import ParseMode
from telegram import Bot
from telegram import ReplyKeyboardMarkup
from telegram import ReplyKeyboardRemove
from telegram.ext import ConversationHandler
from utility import get_keyboard_auth, distance
from utility import SMILE
import qrcode
import os
from datetime import datetime, timedelta
from models import User, Journey
from qrcode.image.pure import PymagingImage

# функция sms() будет вызвана пользователем при отправке команды start,
# внутри функции будет описана логика при ее вызове
def sms(bot, update):
    print(bot.message)
    smile = emojize(choice(SMILE), use_aliases=True)  # для ответа добавили emoji
    print(bot.effective_user)
    print('Кто-то отправил команду /start. Что мне делать?')  # вывод сообщения в консоль при отправки команды /start
    bot.message.reply_text('Здравствуйте, {}! \nПоговорите со мной {}!'
                           .format(bot.message.chat.first_name, smile),
                           reply_markup=get_keyboard_auth())  # отправляем ответ





# функция отправляет случайную картинку

def inline_button_pressed(bot, update):
    print(bot.callback_query)
    query = bot.callback_query  # данные которые приходят после нажатия кнопки
    data = int(query.data)  # получаем данные нажатой кнопки (1.png или -1.png)
    update.bot.edit_message_caption(
        caption='Спасибо вам за ваш выбор!',
        chat_id=query.message.chat.id,
        message_id=query.message.message_id)  # уберем inline клавиатуру выведем текст


def send_qr(bot, update):
    data=update.user_data['phone']
    user, created = User.find_or_create_user(bot.effective_user, bot.message, data)  # получаем данные из базы данных
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
    )
    try:
        passport = update.user_data['passport']
        data = update.user_data['datetime'] + str(user.id)+ passport + update.user_data['type']
        qr.add_data(data)
        qr.make(fit=True)
        color = distance(update)
        img = qr.make_image(fill_color="black", back_color=color)
        img.save(str(bot.message.chat.id)+'.png', 'PNG')
        bot.message.reply_text('Ваш QR Code', reply_markup=get_keyboard_auth())
        update.bot.send_photo(chat_id=bot.message.chat.id, photo=open(str(bot.message.chat.id)+'.png', 'rb'))
        os.remove(str(bot.message.chat.id)+'.png')
        return ConversationHandler.END
    except LookupError:
        passport = ''
        distance(update)
        color = distance(update)
        data = update.user_data['datetime']+ str(user.id)+ passport + update.user_data['type']
        qr.add_data(data)
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color=color)
        img.save(str(bot.message.chat.id) + '.png', 'PNG')
        bot.message.reply_text('Ваш QR Code', reply_markup=get_keyboard_auth())
        update.bot.send_photo(chat_id=bot.message.chat.id, photo=open(str(bot.message.chat.id) + '.png', 'rb'))
        os.remove(str(bot.message.chat.id) + '.png')
        return ConversationHandler.END


# функция парсит анекдоты
def get_anecdote(bot, update):
    receive = requests.get('http://anekdotme.ru/random')  # отправляем запрос к странице
    page = BeautifulSoup(receive.text, "html.parser")  # подключаем html парсер, получаем текст страницы
    find = page.select('.anekdot_text')  # из страницы html получаем class="anekdot_text"
    for text in find:
        page = (text.getText().strip())  # из class="anekdot_text" получаем текс и убираем пробелы по сторонам
    bot.message.reply_text(page)  # отправляем один анекдот, последний


# функция parrot() отвечает темже сообщением которое ему прислали
def parrot(bot, update):
    print(bot.message.text)  # печатаем на экран сообщение пользователя
    bot.message.reply_text(bot.message.text, reply_markup=get_keyboard_auth())  # отправляем обратно текс который пользователь послал


# функция печатает и отвечает на полученный контакт
def get_contact(bot, update):
    update.user_data['phone'] = bot.message.contact['phone_number']
    bot.message.reply_text('{}, мы получили ваш номер телефона!'.format(bot.message.chat.first_name))


# функция печатает и отвечает на полученные геоданные
def get_location(bot, update):
    update.user_data['location'] = bot.message.location
    print(update.user_data['location'])
    bot.message.reply_text('{}, мы получили ваше местоположение!'.format(bot.message.chat.first_name))
    bot.message.reply_text("Location: "+ str(bot.message.location))


def get_data_start(bot, update):
    try:
        data = update.user_data['phone']
        print(data)
        user, created = User.find_or_create_user(bot.effective_user, bot.message, data)  # получаем данные из базы данных
        if user:
            reply_keyboard = [["Фамилия", "Имя"], ['Отчество', 'Email'], ['Продолжить']]  # создаем клавиатуру
            text = """Ваши данные:
                <b>Имя:</b>    {}
                <b>Фамилия:</b> {}
                <b>Отчество:</b> {}
                <b>Email</b> {}

                Заполните пустые строки, измените ваши данные на реальные
                """.format(user.first_name, user.last_name, user.surname, user.email)
            print(text)
            bot.message.reply_text(text, parse_mode=ParseMode.HTML,
                                   reply_markup=ReplyKeyboardMarkup(reply_keyboard,
                                                                    resize_keyboard=True,
                                                                    one_time_keyboard=True))  # вопрос и убираем основную клавиатуру
            return "change"
    except LookupError:
        bot.message.reply_text('Вы не указали свой номер телефона, пожалуйста, нажмите кнопку')
        return ConversationHandler.END


def choice_change(bot, update):
    reply_keyboard = [["Пропуск в магазин"], ["Рабочий пропуск"], ['Одноразовый пропуск']]
    if bot.message.text == 'Имя':
        bot.message.reply_text("Введите новое имя")
        return 'user_name'
    if bot.message.text == 'Фамилия':
        bot.message.reply_text("Введите новую фамилию")
        return 'user_lastname'
    if bot.message.text == 'Отчество':
        bot.message.reply_text("Введите новое отчество")
        return 'user_surname'
    if bot.message.text == 'Email':
        bot.message.reply_text("Введите новый Email")
        return 'user_email'
    if bot.message.text == 'Продолжить':
        bot.message.reply_text("Выберите тип пропуска",
                               reply_markup=ReplyKeyboardMarkup(
                                   reply_keyboard, resize_keyboard=True, one_time_keyboard=True))
        return 'continue'


def anketa_get_name(bot, update):
    reply_keyboard = [["Фамилия", "Имя"],['Отчество', 'Email'], ['Продолжить']]
    data = update.user_data['phone']
    user, created = User.find_or_create_user(bot.effective_user, bot.message, data)
    user.first_name = bot.message.text  # временно сохраняем ответ
    user.save()
    text = """Ваши данные:
    <b>Имя:</b>    {}
    <b>Фамилия:</b> {}
    <b>Отчество:</b> {}
    <b>Email:</b> {}

            Вы хотите изменить какие-то данные?
            """.format(user.first_name, user.last_name, user.surname, user.email)
    bot.message.reply_text(
        text, parse_mode=ParseMode.HTML, reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, resize_keyboard=True, one_time_keyboard=True))
    return "change"  # ключ для определения следующего шага


def anketa_get_surname(bot, update):
    reply_keyboard = [["Фамилия", "Имя"],['Отчество', 'Email'], ['Продолжить']]
    data = update.user_data['phone']
    user, created = User.find_or_create_user(bot.effective_user, bot.message, data)
    user.surname = bot.message.text  # временно сохраняем ответ
    user.save()
    text = """Ваши данные:
    <b>Имя:</b> {}
    <b>Фамилия:</b> {}
    <b>Отчество:</b> {}
    <b>Email:</b> {}
    Вы хотите изменить какие-то данные?
    """.format(user.first_name, user.last_name, user.surname, user.email)
    bot.message.reply_text(
        text, parse_mode=ParseMode.HTML, reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, resize_keyboard=True, one_time_keyboard=True))
    return "change"  # ключ для определения следующего шага


def anketa_get_last_name(bot, update):
    reply_keyboard = [["Фамилия", "Имя"],['Отчество', 'Email'], ['Продолжить']]
    data = update.user_data['phone']
    user, created = User.find_or_create_user(bot.effective_user, bot.message, data)
    user.last_name = bot.message.text  # временно сохраняем ответ
    user.save()
    text = """Ваши данные:
    <b>Имя:</b> {}
    <b>Фамилия:</b> {}
    <b>Отчество:</b> {}
    <b>Email:</b> {}
    Вы хотите изменить какие-то данные?
    """.format(user.first_name, user.last_name, user.surname, user.email)
    bot.message.reply_text(
        text, parse_mode=ParseMode.HTML, reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, resize_keyboard=True, one_time_keyboard=True))
    return "change"  # ключ для определения следующего шага


def anketa_get_email(bot, update):
    reply_keyboard = [["Фамилия", "Имя"],['Отчество', 'Email'], ['Продолжить']]
    data = update.user_data['phone']
    user, created = User.find_or_create_user(bot.effective_user, bot.message, data)
    user.email = bot.message.text  # временно сохраняем ответ
    user.save()
    text = """Ваши данные:
    <b>Имя:</b> {}
    <b>Фамилия:</b> {}
    <b>Отчество:</b> {}
    <b>Email:</b> {}
    Вы хотите изменить какие-то данные?
    """.format(user.first_name, user.last_name, user.surname, user.email)
    bot.message.reply_text(
        text, parse_mode=ParseMode.HTML, reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, resize_keyboard=True, one_time_keyboard=True))
    return "change"  # ключ для определения следующего шага


def choice_type(bot, update):
    data = update.user_data['phone']
    user, created = User.find_or_create_user(bot.effective_user, bot.message, data)
    reply_keyboard = [["Фамилия", "Имя"],['Отчество', 'Email'], ['Продолжить']]  # создаем клавиатуру
    if user.last_name != '' and user.surname != '' and user.email != '':
        if bot.message.text == 'Пропуск в магазин':
            bot.message.reply_text('Укажите место магазина, нажмите "добавить/скрепка" затем "геопозиция" и перетащите маркер на место магазина')
            update.user_data['type']='street'
            return "location"  # ключ для определения следующего шага
        if bot.message.text == "Рабочий пропуск":
            bot.message.reply_text('Укажите пасспортные данные, они будут удалены из переписки после проверки"')
            update.user_data['type']='Work'
            return 'user_data'
        if bot.message.text == 'Одноразовый пропуск':
            bot.message.reply_text('Укажите пасспортные данные, они будут удалены из переписки после проверки"')
            return 'user_data'
    else:
        bot.message.reply_text('Вы заполнини данные о себе не полностью \nПожалуйста, заполните их полностью')
        text = """Ваши данные:
               <b>Имя:</b> {}
               <b>Фамилия:</b> {}
               <b>Отчество:</b> {}
               <b>Email:</b> {}

                Вы хотите изменить какие-то данные?
                        """.format(user.first_name, user.last_name, user.surname, user.email)
        bot.message.reply_text(
            text, parse_mode=ParseMode.HTML, reply_markup=ReplyKeyboardMarkup(
                reply_keyboard, resize_keyboard=True, one_time_keyboard=True))
        return 'change'


def location(bot, update):
    update.user_data['location'] = bot.message.location
    print(update.user_data['location'])
    bot.message.reply_text('Координаты получены')
    if update.user_data['type']=='street':
        bot.message.reply_text('Укажите время и дату посещения магазина')
        bot.message.reply_text('Пример: ДД.ММ.ГГГГ 12:00')
        return 'shop-time'
    if update.user_data['type']=='One-day':
        bot.message.reply_text('Укажите цель поездки')
        return 'aim'
    if update.user_data['type']=='Work':
        bot.message.reply_text('Укажите наименование организации')
        return 'work_name'

def time(bot, update):
    reply_keyboard = [['Получить QR code']]
    data = update.user_data['phone']
    if len(bot.message.text) == 16:
        update.user_data['datetime'] = bot.message.text
        update.user_data['jour_type']='Выход у дома'
        update.user_data['transport']='Legs'
        update.user_data['aim']=''
        save_journey(bot, update, data)
        text = """Вы собираетесь посетить указанное место {datetime}""".format(**update.user_data)
        bot.message.reply_text(text, parse_mode=ParseMode.HTML, reply_markup=ReplyKeyboardMarkup(reply_keyboard))  # текстовое сообщение с форматированием HTML
        return 'send-qr'
    else:
        bot.message.reply_text('Вы указали дату и время в неверном формате, будте внимательны')
        bot.message.reply_text('Укажите время и дату посещения магазина')
        bot.message.reply_text('Пример: ДД.ММ.ГГГГ 12:00')
        return 'shop-time'

def save_journey(bot, update, data):
    time = update.user_data['datetime']
    location = update.user_data['location']
    user, created = User.find_or_create_user(bot.effective_user, bot.message, data)  # получаем данные из базы данных
    journey = Journey()
    print(user)
    try:
        time = datetime.strptime(time, '%d.%m.%Y %H:%M')
        if time < datetime.now():
            journey.time = time
            journey.trans_type =update.user_data['transport']
            journey.type = update.user_data['jour_type']
            journey.aim = update.user_data['aim']
            journey.end_point = location
            journey.start_point = location
            journey.user_id = user.id
            journey.type = update.user_data['type']
            journey.save()
    except ValueError:
        bot.message.reply_text('Вы указали дату и время в неверном формате, будте внимательны')
        bot.message.reply_text('Укажите время и дату посещения магазина')
        bot.message.reply_text('Пример: ДД.ММ.ГГГГ 12:00')
        return 'shop-time'


def anketa_get_persdata(bot, update):
    reply_keyboard = [['Личный транспорт/Служебный транспорт'],['Общественный транспорт']]
    phone=update.user_data['phone']
    data = bot.message.text
    if len(data) == 11:
        user, created = User.find_or_create_user(bot.effective_user, bot.message, phone)
        user.pasport = data
        update.bot.delete_message(chat_id=bot.message.chat.id,message_id=bot.message.message_id)

        bot.message.reply_text('Укажите вид транспорта на котором планируете передвигаться', reply_markup=ReplyKeyboardMarkup(reply_keyboard))
        return 'transport'
    else:
        bot.message.reply_text('Вы указали неверные данные, пожалуйста, проверьте их')
        bot.message.reply_text('Введите данные еще раз')
        update.bot.delete_message(chat_id=bot.message.chat.id,message_id=bot.message.message_id)
        return 'user_data'

def anketa_get_transport(bot, update):
    update.user_data['transport'] = bot.message.text
    bot.message.reply_text('Укажите место в которое вы собираетесь направиться, нажмите "добавить/скрепка" затем "геопозиция" и перетащите маркер на это места')

    return 'location'


def anketa_get_aim(bot, update):
    data = bot.message.text
    jour = Journey()
    jour.aim = data
    data = update.user_data['phone']
    user, created = User.find_or_create_user(bot.effective_user, bot.message, data)

    if update.user_data['type'] == "One-day":
        jour.type = 'Разовая поездка'
    jour.start_point = update.user_data['location']
    jour.end_point = update.user_data['location']
    jour.time = datetime.now()+timedelta(hours=24)
    jour.trans_type = update.user_data['transport']
    jour.user_id = user.id
    jour.save()
    bot.message.reply_text('Ваша заявка будет рассмотрен. \nОтвет будет отправлен на почту')
    return ConversationHandler.END


def anketa_get_workname(bot, update):
    if bot.message.text!="":
        update.user_data['work_name'] = bot.message.text
        bot.message.reply_text('Укажите ИНН организации')
        return 'work_inn'
    else:
        bot.message.reply_text('Вы оставили поле пустым, укажите наименование организации')
        return 'work_name'


def anketa_get_workinn(bot, update):
    if bot.message.text!="":
        jour = Journey()
        jour.aim = bot.message.text+ " " + update.user_data['work_name']
        jour.time = jour.time = datetime.strptime('30.04.2020 23:59', '%d.%m.%Y %H:%M')
        data = update.user_data['phone']
        user, created = User.find_or_create_user(bot.effective_user, bot.message, data)
        jour.user_id = user.id
        jour.start_point = update.user_data['location']
        jour.trans_type = update.user_data['transport']
        jour.end_point = update.user_data['location']
        jour.type = 'Цифровой пропуск для рабочих'
        jour.save()
        bot.message.reply_text('Ваша заявка будет рассмотрен. \nОтвет будет отправлен на почту')
        return ConversationHandler.END
    else:
        bot.message.reply_text('Вы оставили поле пустым, укажите наименование организации')
        return 'work_inn'


def dontknow(bot, update):
    bot.message.reply_text("Я вас не понимаю, выберите оценку на клавиатуре!")
